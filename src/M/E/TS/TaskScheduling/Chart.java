/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
 /*created by Mahdi.Ekhlas at 1398/03/16*/
package M.E.TS.TaskScheduling;

import java.util.List;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;

/**
 *
 * @author Mahdi.Ekhlas
 */
public class Chart {

    public List<Double> data;
    private String xLabel;
    private String yLabel;
    private String title;
    private String seriesName;

    public Chart(List<Double> value,String _xLabel,String _yLabel,String _title,String _seriesName) {
        data = value;
        xLabel = _xLabel;
        yLabel = _yLabel;
        title = _title;
        seriesName = _seriesName;
    }

    public LineChart Run() {
        NumberAxis xAxis;
        NumberAxis yAxis;
        javafx.scene.chart.LineChart<Number, Number> lineChart;

        XYChart.Series Series;

        // define the axes
        xAxis = new NumberAxis();
        yAxis = new NumberAxis();
        xAxis.setLabel(xLabel);
        yAxis.setLabel(yLabel);
        // create line chart
        lineChart = new javafx.scene.chart.LineChart<Number, Number>(xAxis, yAxis);

        lineChart.setTitle(title);
        Series = new XYChart.Series();
        Series.setName(seriesName);
        for (int i = 0; i < data.size(); i++) {
            Series.getData().add(new XYChart.Data(i, (data.get(i))));
        }

        lineChart.getData().add(Series);
        return lineChart;

    }

}
