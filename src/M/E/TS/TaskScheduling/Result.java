/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
 /*created by Mahdi.Ekhlas at 1398/03/16*/
package M.E.TS.TaskScheduling;

import M.E.TS.TaskScheduling.GA.Individual;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Mahdi.Ekhlas
 */
public class Result {
    private List<Integer> bestMapCloudLetToVm = null;
    private List<Double> bestFitness = null;
    public Result(List<Integer> _bestcldbindtovm,List<Double> _bestfitness){
        bestMapCloudLetToVm = new ArrayList<Integer>();
        bestFitness = new ArrayList<Double>();
        bestMapCloudLetToVm.addAll(_bestcldbindtovm);
        bestFitness.addAll(_bestfitness);
    }
    public List<Integer> getbestbinding(){
        return bestMapCloudLetToVm;
    }

    public List<Double> getbestfitness(){
        return bestFitness;
    }
}
