/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
 /*created by Mahdi.Ekhlas at 1397/11/28*/
package M.E.TS.TaskScheduling.GA;

/**
 *
 * @author Mahdi.Ekhlas
 */
import java.util.List;
import java.io.File;
import org.dom4j.*;
import org.dom4j.io.SAXReader;
import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.Cloudlet;

public class Parameter {

    private String useOption = "";
    private int percentage = 0;
    private int populationSize = 0;
    private int generationSize = 0;
    private double tournamentRate = 0.00;
    private int tournamentSize = 0;
    private double crossoverRate = 0.00;
    private double mutationRate = 0.00;
    private String selectionPolicy = "";
    private String crossoverPolicy = "";
    private String mutationPolicy = "";
    private List<Vm> vmsList = null;
    private List<Cloudlet> cloudletsList = null;
    Document docConfig = null;

    public Parameter(String filename) {
        try {
            parse(filename);
        } catch (DocumentException e) {
            e.printStackTrace();
            //TODO: throw something back to the caller
        }
    }

    public void Apply(List<Vm> vmlist, List<Cloudlet> ctlist, boolean usePercentage) {
        vmsList = vmlist;
        cloudletsList = ctlist;
        if (usePercentage) {
            populationSize = cloudletsList.size() * percentage / (3 * 100);
            generationSize = cloudletsList.size() * percentage / (3 * 100);

        }
        generationSize = generationSize < 100 ? 100 : generationSize;
        
    }

    private void parse(String filename) throws DocumentException {
        File inFile = new File(filename);
        SAXReader reader = new SAXReader();
        docConfig = reader.read(inFile);
        String pText = "";

        Node root = docConfig.selectSingleNode("//parameters");

        Node tmpNode = root.selectSingleNode("option");
        if (tmpNode == null) {
            useOption = "";
        } else {
            useOption = tmpNode.getText();
        }

        pText = root.selectSingleNode("population/size").getText();
        populationSize = Integer.parseInt(pText);

        pText = root.selectSingleNode("population/generation").getText();
        generationSize = Integer.parseInt(pText);

        pText = root.selectSingleNode("population/percentage").getText();
        percentage = Integer.parseInt(pText);

        pText = root.selectSingleNode("tournament/probability").getText();
        tournamentRate = Double.parseDouble(pText);

        pText = root.selectSingleNode("tournament/size").getText();
        tournamentSize = Integer.parseInt(pText);

        selectionPolicy = root.selectSingleNode("selection/policy").getText();

        crossoverPolicy = root.selectSingleNode("crossover/policy").getText();
        pText = root.selectSingleNode("crossover/rate").getText();
        crossoverRate = Double.parseDouble(pText);

        mutationPolicy = root.selectSingleNode("mutation/policy").getText();
        pText = root.selectSingleNode("mutation/rate").getText();
        mutationRate = Double.parseDouble(pText);
    }

    public List<Vm> GetVMs() {
        return vmsList;
    }

    public List<Cloudlet> GetCloudlets() {
        return cloudletsList;
    }

    public int IncreasePopulationSize(int len) {
        populationSize += len;
        return populationSize;
    }

    public int GetPopulationSize() {
        return populationSize;
    }

    public int GetTournamentSize() {
        return tournamentSize;
    }

    public int SetTournamentSize(int pp) {
        tournamentSize = pp;
        return tournamentSize;
    }

    public double GetTournamentRate() {
        return tournamentRate;
    }

    public double SetTournamentRate(double pp) {
        tournamentRate = pp;
        return tournamentRate;
    }

    public String GetSelectionPolicy() {
        return selectionPolicy;
    }

    public String SetSelectionPolicy(String sepo) {
        selectionPolicy = sepo;
        return selectionPolicy;
    }

    public String GetCrossoverPolicy() {
        return crossoverPolicy;
    }

    public String GetMutationPolicy() {
        return mutationPolicy;
    }

    public double GetCrossoverRate() {
        return crossoverRate;
    }

    public double GetMutationRate() {
        return mutationRate;
    }

    public int GetGenerationSize() {
        return generationSize;
    }

    /* for EFFICIENT test (temporary) */
    public int GetOption() {
        int currentOpt = 0;
        switch (useOption) {
            case "ga":
                currentOpt = 1;
                SetSelectionPolicy("Randomly");
                break;
            case "btga":
                currentOpt = 2;
                SetSelectionPolicy("Best-Two");
                break;
            case "rwga":
                currentOpt = 3;
                SetSelectionPolicy("Roulette-Wheel");
                break;
            case "tsga":
                currentOpt = 4;
                SetSelectionPolicy("Tournament");
                break;
            case "tsga-A":
                currentOpt = 5;
                SetSelectionPolicy("Tournament");
                break;
            default:
                break;
        }

        return currentOpt;
    }

    public String SetOption(String opt) {
        useOption = opt;
        return useOption;
    }
}
