/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
 /*created by Mahdi.Ekhlas at 1397/11/28*/
package M.E.TS.TaskScheduling.GA;

/**
 *
 * @author Mahdi.Ekhlas
 */
import M.E.TS.TaskScheduling.Result;
import java.util.List;
import java.util.ArrayList;
import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.Cloudlet;

public class Scheduler {

    List<Vm> vmList = null;
    List<Cloudlet> ctList = null;
    Parameter parameters = null;

    public Scheduler(List<Vm> vmlist, List<Cloudlet> ctlist) {
        parameters = new Parameter("etc/ga-algorithm-parameter.xml");
        parameters.Apply(vmlist, ctlist, true);
    }
    //-----------------------------------------------------------
    public Individual BestSol = null;
    // Array To Hold Best Fits
    public List<Individual> BestFits = new ArrayList<Individual>();

    public List<Double> getFits() {
        List<Double> Temp = new ArrayList<Double>();
        for (Individual c : BestFits) {
            Temp.add(c.Fitness());
        }
        return Temp;
    }

    //-----------------------------------------------------------
    public Result Execute() {
        Population lastpop = null;
        Population nextpop = null;
        Individual bestSolution = null;
        Individual currSolution = null;
        int counter = 0;

        lastpop = new Population(parameters);
        lastpop.Initialize(); //allocate the optimum

        bestSolution = lastpop.FindTheBest(lastpop.GetIndividuals());
        //-----------------------------------------------------------
        BestSol = lastpop.FindTheBest(lastpop.GetIndividuals());//.Duplicate(bestSolution);
        BestFits.add(BestSol.Duplicate());
        //-----------------------------------------------------------
        while (counter < parameters.GetGenerationSize()) {
            nextpop = lastpop.Evolve();
            currSolution = nextpop.FindTheBest(nextpop.GetIndividuals());
            //-----------------------------------------------------------
            bestSolution = currSolution.Duplicate();
            BestFits.add(currSolution.Duplicate());
            //-----------------------------------------------------------
//            if (currSolution.Fitness() > bestSolution.Fitness()) {
//                bestSolution = currSolution.Duplicate();
//                counter = 0; //reset
//            } else {
//                counter += 1;
//            }
            counter += 1;
            lastpop = nextpop;
        }

        System.out.println("");
        System.out.println("");
        System.out.println("");
        System.out.println("########## Final Result: age=(" + counter + ") ###########");
        System.out.println("");
        bestSolution.Show();
        Result _result = new Result(bestSolution.Return(), this.getFits());

        return _result;
    }
}
