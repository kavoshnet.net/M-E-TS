/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
 /*created by Mahdi.Ekhlas at 1398/03/16*/
package M.E.TS.TaskScheduling.HYBRID;

/**
 *
 * @author Mahdi.Ekhlas
 */
import M.E.TS.TaskScheduling.BBO.*;
import M.E.TS.TaskScheduling.Result;
import java.util.List;
import java.util.ArrayList;
import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.Cloudlet;

public class Scheduler {

    List<Vm> vmList = null;
    List<Cloudlet> ctList = null;
    Parameter parameters = null;

    public Scheduler(List<Vm> vmlist, List<Cloudlet> ctlist) {
        parameters = new Parameter("etc/hybrid-algorithm-parameter.xml");
        parameters.Apply(vmlist, ctlist, true);
    }
//-----------------------------------------------------------
    public Habitat BestSol = null;
    // Array To Hold Best Fits
    public List<Habitat> BestFits = new ArrayList<Habitat>();

    public List<Double> getFits() {
        List<Double> Temp = new ArrayList<Double>();
        for (Habitat c : BestFits) {
            Temp.add(c.getFit());
        }
        return Temp;
    }

    //-----------------------------------------------------------
    public Result Execute() {
        Population _Pop = null;
        _Pop = new Population(parameters);
        int counter = 0;
        
        
//        M.E.TS.TaskScheduling.GA.Scheduler Scheduler = 
//                new M.E.TS.TaskScheduling.GA.Scheduler(parameters.GetVMs(), parameters.GetCloudlets());
//        Result _res = Scheduler.Execute();
//
//        _Pop.Initialize_From(_res.getbestbinding());

        _Pop.Initialize();


//        PrintAns.logPopulation(_Pop, "popinit");
        // Sort Population
        _Pop.Sort();
//        PrintAns.logPopulation(_Pop, "popinitsort");
        // Best Solution Ever Found
        BestSol = new Habitat(_Pop.BestSol().getPosition(), _Pop.BestSol().Fit);

        List<Double> lambda = new ArrayList<Double>();
        lambda = parameters.lambda();

        List<Double> mu = new ArrayList<Double>();
        mu = parameters.mu();

        // BBO Main Loop
        for (int it = 0; it < parameters.GetMaxIt(); it++) {
            // Migration
            Population _NewPop = new Population(parameters);
            _NewPop.addPops(_Pop.getPops());
            for (int i = 0; i < parameters.GetnPop(); i++) {
                for (int k = 0; k < parameters.GetnVar(); k++) {
                    // Migration
                    if (Utility.rand() <= lambda.get(i)) {
                        // Emmigratiom Probabilites
                        List<Double> EP = new ArrayList<Double>();
                        EP.addAll(mu);
                        EP.set(i, 0.0);
                        EP = Utility.normalize(EP);
                        // Select Source Habitat
                        int j = Utility.rouletteWheelSelection(EP);
                        // Migration
                        _NewPop = _NewPop.New_Migrate(i, j, k, _Pop);
//                        PrintAns.logPopulation(_NewPop, "newpopMI___i_" + i + "__j_" + j + "__k_" + k);

                    }
                    // Mutatiom
                    if (Utility.rand() <= parameters.GetPMutation()) {
//                        _NewPop = _NewPop.Mutate(i, k, _Pop);
                        _NewPop = _NewPop.New_Mutate(i, k, _Pop);
//                        PrintAns.logPopulation(_NewPop, "newpopMU___i_" + i + "__k_" + k);
                    }
                }
                // Evaluation
                _NewPop.getPop(i).getPosition().Evaluate();
//                        setFit(FitFunction.getFit(_NewPop.getPop(i).getPosition()));
            }
            // Sort New Population
            _NewPop.Evolve();
            _NewPop.Sort();
//            PrintAns.logPopulation(_NewPop, "Newpop" + it);
            // Select Next Iteration Population
            counter += 1;
            _Pop = _Pop.Merg(_NewPop);

            _Pop.Evolve();            // Sort New Population
//            PrintAns.logPopulation(_Pop, "popmerg" + it);
            _Pop.Sort();
//            PrintAns.logPopulation(_Pop, "popmergsort" + it);
            // Best New Solution Ever Found
            BestSol = _Pop.BestSol();
            // Store Best Fit Ever Found
            BestFits.add(it, BestSol);
            PrintAns.PrintBestFit(it, BestSol);
            if (checkExit(BestFits, it, BestSol)) {
                break;
            }

//        PrintAns.PrintBestPop(BestSol);
        }
//        PrintAns.PrintBestPop(BestSol);

        System.out.println("");
        System.out.println("");
        System.out.println("");
        System.out.println("########## Final Result: age=(" + counter + ") ###########");
        System.out.println("");
        BestSol.Show();
        Result _result = new Result(BestSol.Return(), this.getFits());

        return _result;
    }

    private Boolean checkExit(List<Habitat> BestFits, int it, Habitat BestSol) {
        int equal = 0;
        for (Habitat h : BestFits) {
            if (h.getFit() == BestSol.getFit()) {
                equal += 1;
            }
        }
        return equal > 10;

    }

}
