/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
 /*created by Mahdi.Ekhlas at 1398/03/16*/
package M.E.TS.TaskScheduling.BBO;

/**
 *
 * @author Mahdi.Ekhlas
 */
import java.util.List;
import java.io.File;
import org.dom4j.*;
import org.dom4j.io.SAXReader;
import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.Cloudlet;

public class Parameter {

    private String useOption = "";
    private int percentage = 0;
    private int nPop = 0;               // Number Of Habitats (Population Size)
    private int maxIt = 0;              // Maximum Number Of Iterations
    private int nVar = 0;               // Number Of Decision Variable
//    private double tournamentRate = 0.00;
//    private int tournamentSize = 0;
    private double crossoverRate = 0.00;    //Crossover Rate
    private double pMutaion = 0.00;
    private double keepRate = 0.00;         // Keep Rate
    private double alpha = 0.00;
//    private String selectionPolicy = "";
    private String crossoverPolicy = "";
    private String mutationPolicy = "";
    private List<Vm> vmsList = null;
    private List<Cloudlet> cloudletsList = null;
    Document docConfig = null;

    public Parameter(String filename) {
        try {
            parse(filename);
        } catch (DocumentException e) {
            e.printStackTrace();
            //TODO: throw something back to the caller
        }
    }

    public void Apply(List<Vm> vmlist, List<Cloudlet> ctlist, boolean usePercentage) {
        vmsList = vmlist;
        cloudletsList = ctlist;
        if (usePercentage) {
            nPop = cloudletsList.size() * percentage / (3 * 100);
            maxIt = cloudletsList.size() * percentage / (3 * 100);
            nVar = cloudletsList.size() * (percentage * 2) / 100;

        }
        maxIt = maxIt < 100 ? 100 : maxIt;

    }

    private void parse(String filename) throws DocumentException {
        File inFile = new File(filename);
        SAXReader reader = new SAXReader();
        docConfig = reader.read(inFile);
        String pText = "";

        Node root = docConfig.selectSingleNode("//parameters");

        Node tmpNode = root.selectSingleNode("option");
        if (tmpNode == null) {
            useOption = "";
        } else {
            useOption = tmpNode.getText();
        }

        pText = root.selectSingleNode("population/npop").getText();
        nPop = Integer.parseInt(pText);

        pText = root.selectSingleNode("population/maxit").getText();
        maxIt = Integer.parseInt(pText);

        pText = root.selectSingleNode("population/percentage").getText();
        percentage = Integer.parseInt(pText);
//        pText = root.selectSingleNode("tournament/probability").getText();
//        tournamentRate = Double.parseDouble(pText);
//        pText = root.selectSingleNode("tournament/size").getText();
//        tournamentSize = Integer.parseInt(pText);
//        selectionPolicy = root.selectSingleNode("selection/policy").getText();
        crossoverPolicy = root.selectSingleNode("crossover/policy").getText();
        pText = root.selectSingleNode("crossover/rate").getText();
        crossoverRate = Double.parseDouble(pText);

        mutationPolicy = root.selectSingleNode("mutation/policy").getText();
        pText = root.selectSingleNode("mutation/pmutation").getText();
        pMutaion = Double.parseDouble(pText);

        pText = root.selectSingleNode("population/keeprate").getText();
        keepRate = Double.parseDouble(pText);

        pText = root.selectSingleNode("population/nvar").getText();
        nVar = Integer.parseInt(pText);

        pText = root.selectSingleNode("population/migrationrate").getText();
        alpha = Double.parseDouble(pText);
    }

    public List<Vm> GetVMs() {
        return vmsList;
    }

    public List<Cloudlet> GetCloudlets() {
        return cloudletsList;
    }

    public int IncreasePopulationSize(int len) {
        nPop += len;
        return nPop;
    }

    public int GetnPop() {
        return nPop;
    }

    public int GetnPercentage() {
        return percentage;
    }

    public int GetnVar() {
        return nVar;
    }

    public double Getalpha() {
        return alpha;
    }
//    public int GetTournamentSize() {
//        return tournamentSize;
//    }
//
//    public int SetTournamentSize(int pp) {
//        tournamentSize = pp;
//        return tournamentSize;
//    }
//
//    public double GetTournamentRate() {
//        return tournamentRate;
//    }
//
//    public double SetTournamentRate(double pp) {
//        tournamentRate = pp;
//        return tournamentRate;
//    }
//
//    public String GetSelectionPolicy() {
//        return selectionPolicy;
//    }
//
//    public String SetSelectionPolicy(String sepo) {
//        selectionPolicy = sepo;
//        return selectionPolicy;
//    }

    public String GetCrossoverPolicy() {
        return crossoverPolicy;
    }

    public String GetMutationPolicy() {
        return mutationPolicy;
    }

    public double GetCrossoverRate() {
        return crossoverRate;
    }

    public double GetPMutation() {
        return pMutaion;
    }

    public int GetMaxIt() {
        return maxIt;
    }

    /* for EFFICIENT test (temporary) */
    public int GetOption() {
        int currentOpt = 0;
        switch (useOption) {
            case "BBO":
                currentOpt = 1;
//                SetSelectionPolicy("Randomly");
                break;
//            case "btga":
//                currentOpt = 2;
//                SetSelectionPolicy("Best-Two");
//                break;
//            case "rwga":
//                currentOpt = 3;
//                SetSelectionPolicy("Roulette-Wheel");
//                break;
//            case "tsga":
//                currentOpt = 4;
//                SetSelectionPolicy("Tournament");
//                break;
//            case "tsga-A":
//                currentOpt = 5;
//                SetSelectionPolicy("Tournament");
//                break;
            default:
                break;
        }

        return currentOpt;
    }

    public String SetOption(String opt) {
        useOption = opt;
        return useOption;
    }

    public int nKeep() {                          // Number Of Kept Habitats
        return (int) Math.round(keepRate * nPop);
    }

    public int nNew() {                           // Number Of New Habitats
        return (int) nPop - nKeep();
    }

    public List<Double> mu() {                   // Emmigration Rates
        return Utility.linspace(1, 0, nPop);
    }

    public List<Double> lambda() {               //Immigration Rates
        return Utility.linspace(0, 1, nPop);
    }

    public int varMax() {
        return vmsList.size();
    }

    public int varMin() {
        return vmsList.size() - vmsList.size() + 1;
    }

    public double sigma() {
        return 0.02 * (varMax() - varMin());
    }

}
