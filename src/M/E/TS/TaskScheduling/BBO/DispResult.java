/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
 /*created by Mahdi.Ekhlas at 1398/03/16*/
package M.E.TS.TaskScheduling.BBO;

import M.E.TS.TaskScheduling.Chart;
import M.E.TS.TaskScheduling.ApplicationBBO;
import M.E.TS.TaskScheduling.Establishment;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.cloudbus.cloudsim.Cloudlet;
import org.cloudbus.cloudsim.DatacenterBroker;
import org.cloudbus.cloudsim.Log;
import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.core.CloudSim;

/**
 *
 * @author Mahdi.Ekhlas
 */
public class DispResult extends Application {

    @Override
    public void start(Stage stage) {
        List<Cloudlet> cloudletList = null;
        List<Vm> vmList = null;

        boolean traced = false;
        int users = 1;

        Log.printLine("Starting " + M.E.TS.TaskScheduling.ApplicationBBO.class.getName() + "...");

        Calendar calendar = Calendar.getInstance();
        CloudSim.init(users, calendar, traced);

        Establishment myEstConfig = new Establishment("etc/establishment.xml");

        CloudSim.startSimulation();

        List<DatacenterBroker> brokerArray = myEstConfig.datacenterbrokers;
        cloudletList = new LinkedList<Cloudlet>();
        vmList = new LinkedList<Vm>();

        for (int i = 0; i < brokerArray.size(); i++) {
            cloudletList.addAll(brokerArray.get(i).getCloudletReceivedList());
            vmList.addAll(brokerArray.get(i).getVmList());
        }
        CloudSim.stopSimulation();
        Log.printLine(ApplicationBBO.class.getName() + " finished!");

        stage.setTitle("BBo Algorithm");
        Chart linechart;
        linechart = new Chart(myEstConfig.GetFit(),"Iterations","Best-Fit","","BBO Algorithm");
        Scene scene = new Scene(linechart.Run(), 800, 600);
        stage.setScene(scene);
        stage.show();

    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

}
