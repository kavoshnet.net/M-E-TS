/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
 /*created by Mahdi.Ekhlas at 1398/03/16*/
package M.E.TS.TaskScheduling.BBO;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Mahdi.Ekhlas
 */
public class PrintAns {

    public static void PrintBestFit(int i, Habitat BestSol) {
        System.out.println("Iteration " + i + " BestFit = " + BestSol.getFit());

    }

    public static void PrintBestPop(Habitat BestSol) {
//        int i = 0;
//        System.out.format("## Solution[%d]: Duration=%f, Fitness=%f %n",
//                Idx(), Duration(), Fitness());
//        System.out.println(" ");
//
//        for (Entry<Integer, ArrayList<Atom>> entry : geneMap.entrySet()) {
//            double tmpTime = 0.00;
//            System.out.print("VM[" + entry.getKey() + "]: ");
//            for (Atom atom : entry.getValue()) {
//                tmpTime += atom.GetProcTime();
//                System.out.format("%04d ", atom.GetCloudlet().getCloudletId());
//            }
//            System.out.format("; Time=(%.4f)", tmpTime);
//            System.out.println(" ");
//        }

        ArrayList<Atom> tmpAtomList = null;
//        System.out.println("Best Population  " + BestSol.getPosition() + " With BestFit = " + BestSol.getFit());
        System.out.println("[");
        for (Map.Entry<Integer, ArrayList<Atom>> entry : BestSol.getPosition().GetGeneMap().entrySet()) {
            double tmpTime = 0.00;
            System.out.print("VM[" + entry.getKey() + "]: ");
            System.out.print("(");
            tmpAtomList = entry.getValue();
            for (Atom atom : tmpAtomList) {
                tmpTime += atom.GetProcTime();
                System.out.print(atom.GetCloudlet().getCloudletId() + ",");
            }
            System.out.print(")");
            System.out.format("; Time=(%.4f)", tmpTime);
            System.out.println();
        }
        System.out.print("]");
        System.out.print("\t\tFit=" + BestSol.getFit());
        System.out.println();

    }

//    public static void logPopulation(Population pop, String fileName) {
//        try ( PrintWriter writer = new PrintWriter("log/" + fileName + ".txt", "UTF-8")) {
//            for (int i = 0; i < pop.getPops().size(); i++) {
//                writer.print("Iter=" + (i + 1) + " ");
//                for (int j = 0; j < pop.getPop(i).Position.GetAtoms().size(); j++) {
//                    writer.print("("
//                            + pop.getPop(i).Position.GetAtoms().get(j).GetVM().getId()
//                            + ","
//                            + pop.getPop(i).Position.GetAtoms().get(j).GetCloudlet().getCloudletId()
//                            + ","
//                            + pop.getPop(i).Position.GetAtoms().get(j).GetCloudlet().getVmId()
//                            + ")"
//                    );
//                }
//                writer.print("\t\tFit=" + pop.getPop(i).getFit());
//                writer.println();
//            }
//        } catch (FileNotFoundException ex) {
//            Logger.getLogger(PrintAns.class.getName()).log(Level.SEVERE, null, ex);
//        } catch (UnsupportedEncodingException ex) {
//            Logger.getLogger(PrintAns.class.getName()).log(Level.SEVERE, null, ex);
//        }
//    }
    public static void logPopulation(Population pop, String fileName) {
        ArrayList<Atom> tmpAtomList = null;
        try ( PrintWriter writer = new PrintWriter("log/" + fileName + ".txt", "UTF-8")) {
            for (int i = 0; i < pop.getPops().size(); i++) {
                writer.println("Iter " + (i + 1) + "= ");
                writer.println("[");
                for (Map.Entry<Integer, ArrayList<Atom>> entry : pop.getPop(i).Position.GetGeneMap().entrySet()) {
                    writer.print("<<" + entry.getKey() + ">> ");
                    writer.print("(");
                    tmpAtomList = entry.getValue();
                    for (Atom atom : tmpAtomList) {
                        writer.print(atom.GetCloudlet().getCloudletId() + ",");
                    }
                    writer.print(")");
                    writer.println();
                }
                writer.print("]");
                writer.print("\t\tFit=" + pop.getPop(i).getFit());
                writer.println();
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(PrintAns.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(PrintAns.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
