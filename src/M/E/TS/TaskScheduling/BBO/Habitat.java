/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
 /*created by Mahdi.Ekhlas at 1398/03/16*/
package M.E.TS.TaskScheduling.BBO;

import java.util.Map.Entry;
import java.util.List;
import java.util.ArrayList;
/**
 *
 * @author Mahdi.Ekhlas
 */
public class Habitat {

    public Individual Position = null;
    public double Fit;

    public Habitat() {
    }

    public Habitat(Individual p, double F) {
//        Position = new Individual(p);
//        p=new Individual(p);
        Position = new Individual(p);
        Fit = Position.Evaluate();
//        Fit = F;
    }

    private Habitat(Habitat old) {
        Position = new Individual(old.Position);
        Fit = Position.Evaluate();
//        Position.Duplicate(old.Position);
//        Fit = old.Fit;
    }

    public Habitat Duplicate() {
        return new Habitat(this);
    }

    public double getFit() {
        return Fit;
    }

//    public void setFit(double value) {
//        this.Fit = value;
//    }
    public Individual getPosition() {
        return Position;
    }

    public Atom getPosition(int i) {
        return Position.GetAtom(i);
    }

    public void setPosition(Individual value) {
        this.Position.Duplicate(value);

    }

    public void setPosition(int i, Atom value) {
        this.Position.GetAtom(i).set(value);
    }

    public List<Integer> Return() {
        return this.getPosition().Return();
    }
    
    public void Show() {
        int i = 0;
        System.out.format("## Solution[%d]: Duration=%f, Fitness=%f %n",
                this.getPosition().Idx(), this.getPosition().Duration(), this.getPosition().Fitness());
        System.out.println(" ");

        for (Entry<Integer, ArrayList<Atom>> entry : this.getPosition().GetGeneMap().entrySet()) {
            double tmpTime = 0.00;
            System.out.print("VM[" + entry.getKey() + "]: ");
            for (Atom atom : entry.getValue()) {
                tmpTime += atom.GetProcTime();
                System.out.format("%04d ", atom.GetCloudlet().getCloudletId());
            }
            System.out.format("; Time=(%.4f)", tmpTime);
            System.out.println(" ");
        }
    }
    

}
