/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package M.E.TS.TaskScheduling.BBO;

import org.cloudbus.cloudsim.Cloudlet;
import org.cloudbus.cloudsim.Vm;

/**
 *
 * @author Mahdi.Ekhlas
 */
public class Atom {//implements Clonable{

    private int Id = 0;
    private Vm resource = null;
    private Cloudlet task = null;
    private String binString = "";
    private double procTime = 0.00;

    public Atom(Vm k, Cloudlet s) {
        resource = k;
        task = s;
        Id = s.getCloudletId();
    }

    private Atom(Atom old) {
        Id = old.Id;
        resource = old.resource;
        task = old.task;
        binString = old.binString;
        procTime = old.procTime;
    }

    public Atom Duplicate() {
        return new Atom(this);
    }

    public int GetId() {
        return Id;
    }

    /*
		public void SetId(iid){
			Id = iid;
		}
     */
    public Vm GetVM() {
        return resource;
    }

    public void SetVM(Vm k) {
        resource = k;
    }

    public Cloudlet GetCloudlet() {
        return task;
    }

    public double GetProcTime() {
        return procTime;
    }

    public double Estimate() {
        Vm tmpVm = GetVM();
        Cloudlet tmpCt = GetCloudlet();
        procTime = tmpCt.getCloudletLength() / (tmpVm.getMips() * tmpVm.getNumberOfPes());
        return procTime;
    }

    public void SetCloudlet(Cloudlet s) {
        task = s;
    }

    public void Show() {
        System.out.print(GetVM().getId() + "|");
    }

    private String toBinString(int number, int bitscnt) {
        String result = "";
        String fills = "";
        int count = 0;
        while (number > 0) {
            result = (number & 1) + result;
            number >>= 1;
            count++;
        }
        while (count < bitscnt) {
            fills += "0";
            count++;
        }
        result = fills + result;
        return result;
    }

    public String Encode(int length) {
        Vm tmpVm = GetVM();
        Cloudlet tmpCt = GetCloudlet();
        binString += toBinString(tmpVm.getId(), length);
        binString += ",";
        binString += toBinString(tmpCt.getCloudletId(), length);
        return binString;
    }

    public void SwapWith(Atom old) {
        Vm tmpVm = old.resource;
        old.SetVM(this.GetVM());
        this.SetVM(tmpVm);
        return;
    }

    public Atom set(Atom old) {
        Atom tmpAtom = old.Duplicate();
        this.Id = tmpAtom.Id;
        this.binString = tmpAtom.binString;
        this.procTime = tmpAtom.procTime;
        this.resource = tmpAtom.resource;
        this.task = tmpAtom.task;
        return this;
    }
}//end of Atom
