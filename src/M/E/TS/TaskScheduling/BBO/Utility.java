/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
 /*created by Mahdi.Ekhlas at 1398/03/16*/
package M.E.TS.TaskScheduling.BBO;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import org.cloudbus.cloudsim.Vm;

/**
 *
 * @author Mahdi.Ekhlas
 */
public class Utility {

    public static List<Double> linspace(double start, double stop, int n) {
        List<Double> result = new ArrayList<Double>();

        double step = (stop - start) / (n - 1);

        for (int i = 0; i <= n - 2; i++) {
            result.add(start + (i * step));
        }
        result.add(stop);

        return result;
    }

    public static List<Double> unifrnd(int min, int max, int size) {
        List<Double> Temp = new ArrayList<Double>();
        for (int i = 1; i <= size; i++) {
            Temp.add((Math.random() * ((max - min) + 1)) + min);
        }
        return Temp;
    }

    public static double rand() {
        return Math.random();
    }

    public static double randn() {
        Random ran = new Random();
        return ran.nextGaussian();
    }

    public static double sum(List<Double> value) {

        double sum = 0.0;
        for (double i : value) {
            sum += Math.pow(i, 2);
        }
        return sum;
    }

    public static List<Double> normalize(List<Double> value) {

        double sum = sum(value);
        for (int i = 0; i < value.size(); i++) {
            value.set(i, value.get(i) / sum);
        }
        return value;
    }

    public static int intunifrnd(int min, int max) {
        return (int) ((Math.random() * ((max - min) + 1)) + min);
    }

    public static int rouletteWheelSelection(List<Double> wheelProbabilities) {
        Random rng = new Random();
        double[] cumulativeProba = new double[wheelProbabilities.size()];
        cumulativeProba[0] = wheelProbabilities.get(0);
        for (int i = 1; i < wheelProbabilities.size(); i++) {
            double proba = wheelProbabilities.get(i);
            cumulativeProba[i] = cumulativeProba[i - 1] + proba;
        }
        int last = wheelProbabilities.size() - 1;
        double r = rng.nextDouble();
        int selected = Arrays.binarySearch(cumulativeProba, r);
        if (selected < 0) {
            selected = Math.abs(selected + 1);
        }
        int i = selected;
        while (wheelProbabilities.get(i) == 0.0) {
            i--;
            if (i < 0) {
                i = last;
            }
        }
        selected = i;
        return selected;
    }

    public static boolean EQVM(Vm vm1, Vm vm2) {
        return vm1.getMips() == vm2.getMips()
                && vm1.getNumberOfPes() == vm2.getNumberOfPes()
                && vm1.getRam() == vm2.getRam()
                && vm1.getBw() == vm2.getBw()
                && vm1.getVmm().equals(vm2.getVmm());
    }

}
