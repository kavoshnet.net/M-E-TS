/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
 /*created by Mahdi.Ekhlas at 1398/03/16*/
package M.E.TS.TaskScheduling.BBO;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import org.cloudbus.cloudsim.Vm;

/**
 *
 * @author Mahdi.Ekhlas
 */
public class Population {

    List<Habitat> Pop = new ArrayList<Habitat>();
    private Parameter parameters = null;

    public void Initialize() {
        for (int i = 0; i < parameters.GetnPop(); i++) {
            Individual indv = new Individual(parameters.GetVMs(),
                    parameters.GetCloudlets());
//            indv.Evaluate();
            Habitat hbt = new Habitat(indv, indv.Evaluate());
            Pop.add(hbt);
        }
        System.out.println("#### Initial Best Solution: age=(" + 0 + ") ####");
        System.out.println("");
        this.Sort();
        Habitat ibest = new Habitat(this.BestSol().getPosition(), this.BestSol().Fit);
        ibest.Show();
    }

    public void Initialize_From(List<Integer> candidate) {
        Individual tempindv = new Individual(candidate,parameters.GetVMs(),
                    parameters.GetCloudlets());
            Habitat temphbt = new Habitat(tempindv, tempindv.Evaluate());
            Pop.add(temphbt);
        
        for (int i = 0; i < parameters.GetnPop() -1; i++) {
            Individual indv = new Individual(parameters.GetVMs(),
                    parameters.GetCloudlets());
//            indv.Evaluate();
            Habitat hbt = new Habitat(indv, indv.Evaluate());
            Pop.add(hbt);
        }
        System.out.println("#### Initial Best Solution: age=(" + 0 + ") ####");
        System.out.println("");
        this.Sort();
        Habitat ibest = new Habitat(this.BestSol().getPosition(), this.BestSol().Fit);
        ibest.Show();
    }
    public Population() {
        //nothing to do
    }

    public Population(Parameter par) {
        this.parameters = par;
    }

    public List<Habitat> getPops() {
        return Pop;
    }

    public Habitat getPop(int i) {
        return Pop.get(i);
    }

    public void setPops(List<Habitat> value) {
        this.Pop.clear();
        this.Pop.addAll(value);
    }

    public void setPop(int i, Habitat value) {
//        this.Pop.set(i, value.Duplicate());
        this.Pop.get(i).setPosition(value.getPosition());
        this.Pop.get(i).getPosition().Evaluate();
//                setFit(value.Fit);
    }

    public void addPops(List<Habitat> value) {
        for (Habitat val : value) {
            Habitat htemp = val.Duplicate();
            this.Pop.add(htemp);
        }
//        this.Pop.addAll(value);
    }

    public void addPop(int i, Habitat value) {
        this.Pop.add(i, value);
    }

    public boolean Sort() {
        Collections.sort(Pop, new Comparator<Habitat>() {
            @Override
            public int compare(Habitat i1, Habitat i2) {
                return Double.compare(i2.Fit, i1.Fit);
            }
        }
        );

        return true;
    }

//    public void Sort() {
//        Collections.sort(Pop, new HabitatComparator());
//    }
    public Habitat BestSol() {
        return Pop.get(0);
    }

    public Population Merg(Population newPop) {
        Population _tmpPop = new Population(this.parameters);
        int nKeep = parameters.nKeep();
        int nNew = parameters.nNew();
        for (int i = 0; i < nKeep; i++) {
            _tmpPop.addPop(i, this.getPop(i).Duplicate());
        }
        for (int i = nKeep, j = 0; i < nNew + nKeep; i++, j++) {
            _tmpPop.addPop(i, newPop.getPop(j).Duplicate());
//            this.getPop(i).setPosition(newPop.getPop(j).getPosition().Duplicate());
//            this.setPop(i, newPop.getPop(j).Duplicate());
        }
        return _tmpPop;
    }

    public Population Migrate(int i, int j, int k, Population value) {
        Atom hipk = value.getPop(i).getPosition(k).Duplicate();
        Atom hjpk = value.getPop(j).getPosition(k).Duplicate();

//        //--------------------------------------------------//
//        System.out.println();
//        System.out.println("==============================================================================");
//        System.out.println("==============================================================================");
//        System.out.println("OLD DATA");
//        System.out.format("POP(%d) Position(%d) ", i, k);
//        System.out.format("VM ID = (%d) ", vlaue.getPop(i).getPosition(k).GetVM().getId());
//        System.out.format("CL ID = (%d) ", vlaue.getPop(i).getPosition(k).GetCloudlet().getCloudletId());
//        System.out.format("Evaluation = (%.20f)\n", vlaue.getPop(i).getPosition().Evaluate());
//
//        System.out.format("POP(%d) Position(%d) ", j, k);
//        System.out.format("VM ID = (%d) ", vlaue.getPop(j).getPosition(k).GetVM().getId());
//        System.out.format("CL ID = (%d) ", vlaue.getPop(j).getPosition(k).GetCloudlet().getCloudletId());
//        System.out.format("Evaluation = (%.20f)\n", vlaue.getPop(j).getPosition().Evaluate());
//        System.out.println("----------------------------------------------");
//        //--------------------------------------------------//
//        //---------------------------------------------------------------//
//        double ev1i = value.getPop(i).getPosition().Evaluate();
//        value.getPop(i).getPosition().Show();
//        double ev1j = value.getPop(j).getPosition().Evaluate();
//        value.getPop(j).getPosition().Show();
//        //---------------------------------------------------------------//
        this.getPop(i).setPosition(k, hjpk/* + parameters.Getalpha() * (hjpk - hipk)*/);

        this.getPop(i).getPosition().Evaluate();
        this.getPop(j).getPosition().Evaluate();

//        //---------------------------------------------------------------//
//        double ev2i = this.getPop(i).getPosition().Evaluate();
//        this.getPop(i).getPosition().Show();
//        double ev2j = this.getPop(j).getPosition().Evaluate();
//        this.getPop(j).getPosition().Show();
//        //---------------------------------------------------------------//
//        //--------------------------------------------------//
//        System.out.println("NEW DATA");
//        System.out.format("POP(%d) Position(%d) ", i, k);
//        System.out.format("VM ID = (%d) ", this.getPop(i).getPosition(k).GetVM().getId());
//        System.out.format("CL ID = (%d) ", this.getPop(i).getPosition(k).GetCloudlet().getCloudletId());
//        System.out.format("Evaluation = (%.20f)\n", this.getPop(i).getPosition().Evaluate());
//
//        System.out.format("POP(%d) Position(%d) ", j, k);
//        System.out.format("VM ID = (%d) ", this.getPop(j).getPosition(k).GetVM().getId());
//        System.out.format("CL ID = (%d) ", this.getPop(j).getPosition(k).GetCloudlet().getCloudletId());
//        System.out.format("Evaluation = (%.20f)\n", this.getPop(j).getPosition().Evaluate());
//        System.out.println("==============================================================================");
//        System.out.println("==============================================================================");
//        System.out.println();
//        //--------------------------------------------------//
        return this;

    }

    public Population Mutate(int i, int k, Population value) {
        Atom newhipk = value.getPop(i).getPosition(k).Duplicate();
        int size = this.parameters.GetVMs().size();
        int rnd = Utility.intunifrnd(0, size - 1);
        Vm vmold = newhipk.GetVM();
        Vm vmnew = this.parameters.GetVMs().get(rnd);
        while ((rnd == newhipk.GetVM().getId()) && Utility.EQVM(vmold, vmnew)) {
            rnd = Utility.intunifrnd(0, size - 1);
            vmnew = this.parameters.GetVMs().get(rnd);
        }

//        //---------------------------------------------------------------//
//        double ev1 = value.getPop(i).getPosition().Evaluate();
//        value.getPop(i).getPosition().Show();
//        //---------------------------------------------------------------//
        newhipk.SetVM(this.parameters.GetVMs().get(rnd));
//        //--------------------------------------------------//
//        System.out.println();
//        System.out.println("==============================================================================");
//        System.out.println("==============================================================================");
//        System.out.println("OLD DATA");
//        System.out.format("POP(%d) Position(%d) ", i, k);
//        System.out.format("VM ID = (%d) ", value.getPop(i).getPosition(k).GetVM().getId());
//        System.out.format("CL ID = (%d) ", value.getPop(i).getPosition(k).GetCloudlet().getCloudletId());
//        System.out.format("Evaluation = (%.20f)\n", value.getPop(i).getPosition().Evaluate());
//        System.out.println("----------------------------------------------");
//        //--------------------------------------------------//
        this.getPop(i).setPosition(k, newhipk /*+ parameters.sigma() * Utility.randn()*/);

//        //---------------------------------------------------------------//
//        double ev2 = this.getPop(i).getPosition().Evaluate();
//        this.getPop(i).getPosition().Show();
//        //---------------------------------------------------------------//
        this.getPop(i).getPosition().Evaluate();

//        //--------------------------------------------------//
//        System.out.println("NEW DATA");
//        System.out.format("POP(%d) Position(%d) ", i, k);
//        System.out.format("VM ID = (%d) ", this.getPop(i).getPosition(k).GetVM().getId());
//        System.out.format("CL ID = (%d) ", this.getPop(i).getPosition(k).GetCloudlet().getCloudletId());
//        System.out.format("Evaluation = (%.20f)\n", this.getPop(i).getPosition().Evaluate());
//        System.out.println("==============================================================================");
//        System.out.println("==============================================================================");
//        System.out.println();
//        //--------------------------------------------------//
        return this;
    }

    public Population New_Migrate(int i, int j, int k, Population value) {
        Atom hipk = value.getPop(i).getPosition(k).Duplicate();
        Atom hjpk = value.getPop(j).getPosition(k).Duplicate();

//        //--------------------------------------------------//
//        System.out.println();
//        System.out.println("==============================================================================");
//        System.out.println("==============================================================================");
//        System.out.println("OLD DATA");
//        System.out.format("POP(%d) Position(%d) ", i, k);
//        System.out.format("VM ID = (%d) ", vlaue.getPop(i).getPosition(k).GetVM().getId());
//        System.out.format("CL ID = (%d) ", vlaue.getPop(i).getPosition(k).GetCloudlet().getCloudletId());
//        System.out.format("Evaluation = (%.20f)\n", vlaue.getPop(i).getPosition().Evaluate());
//
//        System.out.format("POP(%d) Position(%d) ", j, k);
//        System.out.format("VM ID = (%d) ", vlaue.getPop(j).getPosition(k).GetVM().getId());
//        System.out.format("CL ID = (%d) ", vlaue.getPop(j).getPosition(k).GetCloudlet().getCloudletId());
//        System.out.format("Evaluation = (%.20f)\n", vlaue.getPop(j).getPosition().Evaluate());
//        System.out.println("----------------------------------------------");
//        //--------------------------------------------------//
//        //---------------------------------------------------------------//
//        double ev1i = value.getPop(i).getPosition().Evaluate();
//        value.getPop(i).getPosition().Show();
//        double ev1j = value.getPop(j).getPosition().Evaluate();
//        value.getPop(j).getPosition().Show();
//        //---------------------------------------------------------------//
        this.getPop(i).setPosition(k, hjpk/* + parameters.Getalpha() * (hjpk - hipk)*/);

        this.getPop(i).getPosition().Evaluate();
        this.getPop(j).getPosition().Evaluate();

//        //---------------------------------------------------------------//
//        double ev2i = this.getPop(i).getPosition().Evaluate();
//        this.getPop(i).getPosition().Show();
//        double ev2j = this.getPop(j).getPosition().Evaluate();
//        this.getPop(j).getPosition().Show();
//        //---------------------------------------------------------------//
//        //--------------------------------------------------//
//        System.out.println("NEW DATA");
//        System.out.format("POP(%d) Position(%d) ", i, k);
//        System.out.format("VM ID = (%d) ", this.getPop(i).getPosition(k).GetVM().getId());
//        System.out.format("CL ID = (%d) ", this.getPop(i).getPosition(k).GetCloudlet().getCloudletId());
//        System.out.format("Evaluation = (%.20f)\n", this.getPop(i).getPosition().Evaluate());
//
//        System.out.format("POP(%d) Position(%d) ", j, k);
//        System.out.format("VM ID = (%d) ", this.getPop(j).getPosition(k).GetVM().getId());
//        System.out.format("CL ID = (%d) ", this.getPop(j).getPosition(k).GetCloudlet().getCloudletId());
//        System.out.format("Evaluation = (%.20f)\n", this.getPop(j).getPosition().Evaluate());
//        System.out.println("==============================================================================");
//        System.out.println("==============================================================================");
//        System.out.println();
//        //--------------------------------------------------//
        return this;

    }

    public Population New_Mutate(int i, int k, Population value) {
        Atom maxAtom = value.getPop(i).getPosition().GetMaxAtom();
        Atom minAtom = value.getPop(i).getPosition().GetMinAtom();
        Atom newhipk = maxAtom.Duplicate();
        int size = this.parameters.GetVMs().size();
        newhipk.SetVM(this.parameters.GetVMs().get(minAtom.GetVM().getId()));
        int newpos = this.getPop(i).getPosition().GetAtomPosition(maxAtom);
        this.getPop(i).setPosition(newpos, newhipk /*+ parameters.sigma() * Utility.randn()*/);
        this.getPop(i).getPosition().Evaluate();
        return this;
    }

    public void Evolve() {
        for (int i = 0; i < this.Pop.size(); i++) {
            double t1 = this.Pop.get(i).getPosition().Fitness();
            this.Pop.get(i).getPosition().Evaluate();
            double t2 = this.Pop.get(i).getPosition().Fitness();
            double x = 0.0;

        }
    }
}

//class HabitatComparator implements Comparator<Habitat> {
//
//    @Override
//    public int compare(Habitat h1, Habitat h2) {
//        return h1.Fit > h2.Fit ? -1 : h1.Fit == h2.Fit? 0 : 1;
//    }
//}
